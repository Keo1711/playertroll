package me.keo1711.playertroll;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Troll extends JavaPlugin {

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fakeop"))
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (!player.hasPermission("troll.op")) {
					player.sendMessage("�4[�6FakeOPs�4]" + ChatColor.GOLD
							+ "You do not have permission to use this command.");
					return true;
				}
				if (args.length == 0) {
					sender.sendMessage(ChatColor.RED
							+ "Please specify a player!");
					return true;
				}
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if (target == null) {
					sender.sendMessage(ChatColor.RED + "Could not find player "
							+ args[0] + ".");
					return true;
				}
				target.sendMessage(ChatColor.YELLOW + "You are now op!");
			}
		if (cmd.getName().equalsIgnoreCase("fakedeop")) {
			if (sender instanceof Player) {
				Player player1 = (Player) sender;

				if (!player1.hasPermission("troll.deop")) {
					player1.sendMessage("�4[�6FakeOPs�4]" + ChatColor.GOLD
							+ "You do not have permission to use this command.");
					return true;
				}
				if (args.length == 0) {
					sender.sendMessage(ChatColor.RED
							+ "Please specify a player!");
					return true;
				}
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if (target == null) {
					sender.sendMessage(ChatColor.RED + "Could not find player "
							+ args[0] + ".");
					return true;
				}
				target.sendMessage(ChatColor.YELLOW + "You are no longer op!");
			}
		}

		return true;
	}
}